import React from 'react'

import { useCountries } from '../../hooks/useCountries'

export default function Neighbors() {
  const { neighbors } = useCountries()
  return (
    <div>
      <h2>Neighbors</h2>
      {
        (neighbors.length === 0) ?
          <p>No mutual neighbors were found!</p> :
          <ul>
            {
              neighbors.map((neighbor, index) => (
                <li key={index}>{neighbor}</li>
              ))
            }
          </ul>
      }
    </div>
  )
}