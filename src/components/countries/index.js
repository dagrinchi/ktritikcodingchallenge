import React from 'react'

import { useCountries } from '../../hooks/useCountries'

export default function Countries() {
  const { getCountries, countries, error } = useCountries()
  if (error) return <p>Error! {error}</p>
  if (countries.length === 0) return <p>Loading!</p>
  return (
    <div className="Countries">
      <button onClick={() => {
        getCountries()
      }}>Reload!</button>
      <h2>Selected countries</h2>
      <ul>
        {
          countries.map(({ name }, index) => (
            <li key={index}>{name}</li>
          ))
        }
      </ul>
    </div>
  )
}