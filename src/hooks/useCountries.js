import React from 'react'

import * as api from '../utils/api'
import * as data from '../utils/data'

const Countries = React.createContext()

export const useCountries = () => React.useContext(Countries)

export function CountriesProvider({ children }) {
  const [error, setError] = React.useState()
  const [countries, setCountries] = React.useState([])
  const [neighbors, setNeighbors] = React.useState([])

  async function getCountry(path) {
    try {
      return await api.get(path)
    } catch (error) {
      setError(error.message || error)
    }
  }

  async function getCountries() {
    setCountries([])
    setNeighbors([])
    try {
      const res = await api.get('/countries.json')
      if (res.length > 10) {
        const randomCountries = data.generateRandomIndexes(10, res.length).map((rIndex) => res[rIndex])
        const rCountries = await Promise.all(randomCountries.map(async ({ name, url }) => {
          const country = await getCountry(url)
          if (!country.neighbors) throw Error()
          return ({
            name,
            neighbors: country.neighbors.map(n => n.name)
          })
        }))
        setCountries(rCountries)
      } else {
        throw new Error('No enough results found!')
      }
    } catch (error) {
      setError(error.message || error)
    }
  }

  React.useEffect(() => {
    (async () => {
      await getCountries()
    })()
  }, [])

  React.useEffect(() => {
    if (countries.length > 0) {
      let c1 = []
      for (let i = 0; i < countries.length; i++) {
        const country = countries[i]
        let c2 = [country.name]
        for (let j = 0; j < country.neighbors.length; j++) {
          const neighbor = country.neighbors[j]
          if (countries.filter(c => c.name === neighbor).length > 0)
            c2.push(neighbor)
        }
        if (c2.length > 1)
          c1.push(c2)
      }
      const result = data.removeDuplicates(c1.map(c => c.sort((a, b) => a < b ? -1 : 1).join(', ')))
      setNeighbors(result)
    }
  }, [countries])

  return <Countries.Provider value={{ getCountries, countries, neighbors, error }}>{children}</Countries.Provider>
}