import { CountriesProvider } from './hooks/useCountries'
import { Countries, Neighbors } from './components'

import './App.css'

function App() {
  return (
    <div className="App">
      <CountriesProvider>
        <h1>Ktritik Coding Challenge</h1>
        <p>by: Wilson David Alméciga</p>
        <Countries />
        <Neighbors />
      </CountriesProvider>
    </div>
  )
}

export default App
