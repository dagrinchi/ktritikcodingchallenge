import { BASE_URL } from '../constants'

export async function get(path) {
  try {
    return fetch(`${/^https?:\/\/[^#?\/]+/.test(path) ? '' : BASE_URL}${path}`).then(res => res.json())
  } catch (error) {
    console.warn(error);
    throw error;
  }
}