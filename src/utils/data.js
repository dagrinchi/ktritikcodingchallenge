export function generateRandomIndexes(qty = 10, length = 100) {
  let arr = []
  while (arr.length < qty) {
    let r = Math.floor(Math.random() * length)
    if (arr.indexOf(r) === -1) arr.push(r)
  }
  return arr
}

export function removeDuplicates(arr) {
  return arr.filter((item, pos) => arr.indexOf(item) == pos)
}